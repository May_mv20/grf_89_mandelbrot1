package fractal4;

import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author Joana Mayrin Morales Vargas
 */
public class Panel extends JPanel{

    public Panel() {
    }
     public void paint(Graphics g) {
        Julia j = new Julia();
        j.dibujar(g, this.getWidth(), this.getHeight());
     
     }
    
}
